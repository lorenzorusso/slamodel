package eu.musaproject.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "threatprotocol")
public class ThreatProtocol {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Column(name = "idThreat")
	private String idThreat;
	
	@Column(name = "idProtocol")
	private String idProtocol;
	
	private Boolean selected;
	
	public ThreatProtocol(){}
	
	public String getIdThreat() {
		return idThreat;
	}

	public void setIdThreat(String t) {
		this.idThreat = t;
	}
	
	public String getIdProtocol() {
		return idProtocol;
	}

	public void setIdProtocol(String t) {
		this.idProtocol = t;
	}
	
	public Boolean getSelected() {
		return selected;
	}

	public void setSelected(Boolean selected) {
		this.selected = selected;
	}

}
