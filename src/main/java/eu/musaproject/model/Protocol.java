package eu.musaproject.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "protocols")
public class Protocol {	

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(name = "idprotocol")
	private String idprotocol;
	
	@Column(name = "protocol")
	private String protocol;
	
	@Column(name = "relation")
	private String relation;
	
	@Column(name = "nodelabel")
	private String nodelabel;

	@Column(name = "nodeproperties")
	private String nodeproperties;
	
	private Boolean selected;

	public Protocol(){}
	
	public Long getThreatId() {
		return id;
	}

	public void setIdProtocol(String t) {
		this.idprotocol = t;
	}

	public String getIdProtocol() {
		return idprotocol;
	}
	
	public void setRelation(String t) {
		this.relation = t;
	}

	public String getRelation() {
		return relation;
	}
	
	public String getProtocol() {
		return protocol;
	}

	public void setProtocol(String protocol) {
		this.protocol = protocol;
	}
	
	public String getNodelabel() {
		return nodelabel;
	}

	public void setNodelabel(String nodelabel) {
		this.nodelabel = nodelabel;
	}
	
	public String getProperties() {
		return nodeproperties;
	}

	public void setNodeproperties(String nodeproperties) {
		this.nodeproperties = nodeproperties;
	}

	public Long getId() {
		return id;
	}

	public Boolean getSelected() {
		return selected;
	}

	public void setSelected(Boolean selected) {
		this.selected = selected;
	}
}
