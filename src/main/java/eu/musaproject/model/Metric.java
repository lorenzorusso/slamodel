package eu.musaproject.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

@Entity
@Table(name = "metrics")
public class Metric {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(name = "metric_id", unique = true)
	private String metricId;
	
	@Column(name = "metric_name")
	private String metricName;
	
	@Column(name = "applicability")
	private String applicability;
	
	@Column(name = "metric_description")
	@Type(type="text")
	private String metricDescription;
	
	@Column(name = "formula")
	@Type(type="text")
	private String formula;
	
	@Column(name = "value_range")
	@Type(type="text")
	private String valueRange;

	@Column(name = "unit")
	private String unit;
	
	@Column(name = "value_description")
	@Type(type="text")
	private String valueDescription;
	
	@Column(name = "default_value")
	@Type(type="text")
	private String defaultValue;
	
	@Column(name = "operator")
	@Type(type="text")
	private String operator;
	
	@Column(name = "source")
	private String source;
	
	public Metric(){}
	
	public String getMetricId() {
		return metricId;
	}

	public void setMetricId(String metricId) {
		this.metricId = metricId;
	}

	public String getApplicability() {
		return applicability;
	}

	public void setApplicability(String applicability) {
		this.applicability = applicability;
	}

	public String getMetricDescription() {
		return metricDescription;
	}

	public void setMetricDescription(String metricDescription) {
		this.metricDescription = metricDescription;
	}

	public String getFormula() {
		return formula;
	}

	public void setFormula(String formula) {
		this.formula = formula;
	}

	public String getValueRange() {
		return valueRange;
	}

	public void setValueRange(String valueRange) {
		this.valueRange = valueRange;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public String getValueDescription() {
		return valueDescription;
	}

	public void setValueDescription(String valueDescription) {
		this.valueDescription = valueDescription;
	}

	public String getDefaultValue() {
		return defaultValue;
	}

	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}

	public String getOperator() {
		return operator;
	}

	public void setOperator(String operator) {
		this.operator = operator;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public Long getId() {
		return id;
	}

	public String getMetricName() {
		return metricName;
	}

	public void setMetricName(String metricName) {
		this.metricName = metricName;
	}

	
}
