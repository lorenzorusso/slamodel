package eu.musaproject.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import eu.musaproject.model.Control;

public interface ControlRepository extends JpaRepository<Control, Long> {
	
	Control findByControlId(String controlId);
	
	@Query(value = "SELECT * FROM controls_metrics", nativeQuery = true )
	List<String> getControlMetrics();
	
	@Query(value = "SELECT * FROM controls_questions_control", nativeQuery = true )
	List<String> getControlQuestions();
	
}
