package eu.musaproject.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import eu.musaproject.model.Protocol;
import eu.musaproject.model.Stride;

public interface ProtocolRepository extends JpaRepository<Protocol, Long> {
	
	@Query(value = "SELECT * FROM protocols", nativeQuery = true )
	List<Protocol> getProtocols();
	
	Protocol findByNodeproperties(String nodeproperties);
}
